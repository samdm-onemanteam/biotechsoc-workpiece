#Should we revisit the peer review process?

## Prerequisites

To compile the pdf, the following software should be installed and available on the command line:

* [LuaTex](http://www.luatex.org/) (many standard LaTex installations come with this included)
* [latexmk](https://www.ctan.org/pkg/latexmk/) (should also be included in standard installation)
* [GNU Make](https://www.gnu.org/software/make/)

## Compiling the pdf

(Untested on windows)

Open a console and `cd` to the root directory of this repository, then run

```
make
```

A `build` and `output` directory will be created. The final pdf is in the `output` directory.

Alternatively, if you use a Linux desktop which follows the `XDG` standard (such as default Ubuntu and Fedora desktops), then run

```
make run
```

This will compile and open the pdf immediately.
