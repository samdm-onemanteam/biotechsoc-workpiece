\documentclass[12pt, final]{article}

\usepackage{enumitem}
\usepackage{fontspec}
\setmainfont{Georgia}
\setsansfont{Verdana}

\usepackage{hyperref}
\usepackage{xcolor}
\hypersetup{
    colorlinks,
    linkcolor={red!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}
\usepackage[authoryear,round]{natbib}
\usepackage[obeyFinal]{todonotes}

\setlength{\marginparwidth}{3.5cm}

\newcommand\todoin[2][]{
    \todo[inline, caption={2do}, #1]{
        \begin{minipage}
            {\textwidth-4pt}\normalsize{#2}
        \end{minipage}
    }
}
\renewcommand{\thefootnote}{\textcolor{black}{\fnsymbol{footnote}}}

\title{Should we revisit the peer review process?}
\author{Sam De Meyer}

\date{\today}

\begin{document}

\begin{titlepage}
\makeatletter
\begin{center}
\begin{minipage}[c][19cm][c]{0.90\textwidth}


\begin{center}
    \includegraphics[width=0.35\linewidth]{figures/UGent_logo}~\\
\end{center}

\vspace{0.5cm}

\begin{center}
    \large{\textbf{Workpiece Biotech and Society}}
\end{center}

\rule{\linewidth}{0.5mm}
\begin{center}
    \huge{\textbf{\@title}}
\end{center}
\rule{\linewidth}{0.5mm}


\vspace{0.2cm}

\begin{center}
    \textbf{\@author} \\
    \vspace{0.2cm}
    \@date
\end{center}

\vfill

{\footnotesize
\noindent\textbf{Disclaimer:} The main goal of this text is to bring together
findings from studies and articles concerning peer review and research
misconduct. The findings presented here are not the result of my own research.

} % don't remove/edit the blank line above!

\end{minipage}
\end{center}
\end{titlepage}

\section{Introduction}

Research affects us many ways, it affects what we eat, how we interact with our
environment, how we decide on medical treatments, which habits we try to keep
or avoid, which products/devices we use every day and so on. It affects how we
think about ourselves and our planet and it challenges our morality.  Our
western society relies so much on research and technology that it is save to
say it influences nearly every aspect of our lives.

Yet, erroneous research is being discovered on a regular basis, sometimes
leading to disastrous consequences. Most severe are basing medical treatment
and clinical trials on flawed research \citep{Steen2011} and harming public
opinion such as in the \cite{Wakefield1998} paper, falsely linking autism to
vaccination. Less severe consequences are distrust in the scientific community,
waste of time and money, reputation loss of authors and career breaks
\citep{Stern2014}.

Few systems are currently in use to prevent flawed research from being
published. The peer review process is supposed to be, among other things,
responsible for intercepting flawed research before it reaches a broad public,
but its effectiveness is under heavy debate. It is curious how the peer review
process is at the heart of science, even though there is still no scientific
evidence proving that it actually works.

By preventing flawed research from being published (or failing to do so), the
peer review process can have a big impact on the scientific community and
society in general. Although there are many more aspects to discuss about this
topic, I will focus on the following questions in this article: What gives rise
to flawed research and how does is pass the peer review process? What is peer
review and is it actually responsible for stopping flawed research? What
are the consequences of retracted papers? How can we improve the current
situation?

\section{Investigating flawed research}

A distinction has to be made between flawed research that is due to misconduct
and that which is due to honest mistakes. Research misconduct can include data
falsification and cherry-picking results as well as duplicate article
submissions and plagiarism.  Honest mistakes on the other hand include poor
experimental design or mistakes in data analysis and interpretation. Several
studies have tried to quantify to which extend research misconduct is at the
cause of flawed research as opposed to honest mistakes. Some older studies
claim that honest mistakes take up the bulk of retracted articles
\citep{Nath2006,Wager2011}, but a reanalysis of the data by \cite{Fang2012}
including additional evidence and data showed exactly the opposite. Although it
is difficult to quantify exactly to what extend research misconduct is at the
cause of flawed research, the studies generally agree on some important issues.

\paragraph{The publish-or-perish climate gives rise to flawed research}
Scientific research today is highly competitive due to grants being tied to
scientific output in peer reviewed journals. The more publications a researcher
has, preferably in high-impact journals, the easier it is for him/her to get
additional funding. Because the stakes of getting a high-impact publication are
so high, it is tempting to hasten experiments or to invent false data.
\cite{Stern2014} have investigated this issue in more detail and discovered
that on average, funding for a researcher/group starts to decline several years
before their first fraud is discovered. This strengthens the hypothesis that
decreasing access to funding promotes research misconduct.

\paragraph{Fraud is often repeated multiple times by the same author} Once a
first fraudulent paper is discovered, other work of the same authors is
scrutinized in more detail and often a whole series of article retractions
follow. For example, 390 out of 889 retracted papers in \cite{Fang2012} came
from only 38 research groups. The Japanese cancer researcher Naoki Mori, a
notorious repeat fraudster with over 15 retracted papers, and the Dutch social
psychologist Diederik Stapel with 58 papers currently retracted are prime
examples.

\paragraph{Retraction rates are rising significantly} The earliest paper
retraction found by \cite{Fang2012} dates from 1977 (published in 1974) and the
number of retractions is rising ever since. The rise in the absolute number of
retractions could be explained simply as a function of more and more scientific
journals appearing over time. However, the relative frequency of retraction is
rising as well and has increased 10-fold since 1975. Plagiarism and duplicate
publications seem to be especially on the rise in the last decade, counter to
the fraction of honest mistakes declining slightly over the same time period.
There is no consensus yet whether the rise in retractions is due to more false
research being detected or more false research being created. Luckily, a study
by \cite{Fanelli2013} gives an indication that the latter is more likely.

\paragraph{A large part of flawed research is still undetected} All studies
agree on this topic, and one group has even tried to quantify the total amount
of papers that should have been retracted. According to \cite{Cokol2007} at
least 10.000 papers should be retracted in the period of 1950 up to 2004 in
the most generous scenario; the most pessimistic case estimates
up to 100.000. These are staggering numbers, but the method used for the
estimation is not generally agreed upon by other scientists \citep{Butler2007}.
Yet another paper reports a meta-analysis of surveys asking scientists if they
are involved in or aware of scientific misconduct or questionable research
practices. Roughly 2\% admitted to having committed some type misconduct at
least once in their life and 14\% admitted knowing of data falsification of
colleges \cite{Fanelli2009}.

\paragraph{Geographical origin varies across types of misconduct}
Some countries are more prone to certain types of scientific misconduct than
others.  According to \cite{Fang2012}, United States researchers are more
likely to falsify data while researchers from India or China are more likely to
plagiarise papers.

\paragraph{Impact factor correlates with retraction rate} This phenomenon was
reported by \cite{Fang2011} and \cite{Fang2012}. Although I could not find
studies by other authors confirming this correlation, it seems to be accepted
by most groups researching this topic. At the moment there are two
explanations in circulation: one is that high impact journals receive more
scrutiny due to their large audience and the other is that high impact journals
have better retraction policies.

\paragraph{Flawed research can be researched} It is possible to investigate the
causes an properties of flawed research but it is difficult. One of the first
attempts was done by \cite{Budd1998} using retracted articles as data for the
study. This attempt was followed by the other studies listed above using
roughly the same techniques. Although the introduction of PubMed
(\url{www.ncbi.nlm.nih.gov/pubmed}), the Web of Science
(\url{https://webofknowledge.com}) and the US Office of Research Integrity
(ORI, \url{https://ori.hhs.gov}) have greatly facilitated investigating false
research, problems remain. One major problem being that retraction notes are
often unclear about the exact reason for retraction and authors are,
understandably, a little reluctant towards sharing details concerning a
retraction. Another major problem is which cases to include in the statistical
analysis. Some groups include all papers from Web of Science in their analysis
while other groups restrict their scope to PubMed. Also, some groups only
consider papers that were published after 1992, when the ORI was founded.
These differences in inclusion criteria are often a root cause of conflicting
conclusions.

\section{Consequences of retracted papers}

Several parties are harmed by the discovery of flawed research. In any case the
authors and associated researchers suffer from reputation loss. In addition it
increases distrust in the scientific community, both for insiders and the
general public. It also means time and money are wasted, not only on the making
of the retracted article itself, but also on any other research that is itself
based on results from the retracted article. Generally three major groups are
harmed: 1) the authors and associates from the retracted article, 2) the
scientific community and 3) the general public.

\paragraph{Consequences for authors and associates} It is to be expected that
there is a drop in citations after an article has been retracted. This has been
confirmed, in most cases, by \cite{Stern2014}, but there are exceptions where
retracted articles continue to be cited as valid work \citep{Fang2012}. In
those cases where there was a drop in citations, a drop in funding was observed
as well. In addition, if fraud was at the cause of a paper retraction, it is
often accompanied by a reputation loss, leading to a drop in citations for the
authors prior work as well \citep{Lu2013}.  Finally there is also collateral
damage for the whistleblowers and associates of the author in question.
They also suffer from reputation loss and possibly career-endings (only
anecdotal evidence available: \cite{Nature2010,Couzin2006}).

\paragraph{Consequences for the scientific community} \cite{Stern2014} have
investigated the amount of NIH funding that went into retracted papers and
estimated it to be less than 1\% of total NIH funds. Although it should be
noted that more money is lost than just the research grants, such as the cost
of the investigation leading to the retraction, the loss of future funding and
the waste resources spent on research based on retracted articles.  The latter
has been examined in more detail by \cite{Trikalinos2008}, who concluded that,
before the date of retraction, flawed research is indiscernible from legitimate
research in terms of citation counts. A more subjective consequence is that
discovery of flawed research might create an unhealthy atmosphere of distrust
within the scientific community, but I have not found any research on this
topic.

\paragraph{Consequences for the public} Flawed research also affects
individuals outside of the scientific community.  In some cases public health
has been put in danger, of these the most notorious example is the paper from
\cite{Wakefield1998}, which stated that there is a correlation between
vaccination and autism. This prevented many parents from immunizing their
children, leading to outbreaks of preventable disease shortly after the paper
got published \citep{Flaherty2011a}. In other cases specific individuals were
put at risk, for example in \cite{Potti2011}, where clinical trials for
chemotherapy were started based on falsified data. For an overview of patient
harm by flawed research I refer to \cite{Steen2011}.

Another important consequence is that retracted papers, especially in medical
research, can also create a distrust between the public and the scientific community.

\section{What do we mean by peer review?}

Here are the basics of how the peer review should work. First of all, a
scientist makes a discovery that he/she wants to share with others working in
the same field. To do so the results and methods are summarized in a paper
which is then sent to one of the many available journals. The choice of journal
is determined by two main factors: the topics it covers and the impact factor.
Generally an attempt is made to publish in a journal with the highest impact
factor possible and which is expected to accept the paper. The paper is then
submitted to the chosen journal and evaluated by one of journal's editors. The
editor will then determine if the paper's contents are within the journal's
scope and if there are any obvious flaws. The editor can then either reject the
paper or pass it on to two or more reviewers. The reviewers are expected to
read the paper in great detail, to question it's methods and results as well as
the novelty of the findings. A reviewer can then either recommend to reject the
paper entirely or ask for additional experiments or clarifications. Only in
rare cases is the paper immediately accepted by all reviewers. The editor will
then take the comments of the reviewers into account to either accept the
paper, ask the authors for resubmission after addressing the reviewers comments
or reject it. Usually one or more rounds of submissions follow until all
reviewers agree the paper should be published without further amendments.  When
the paper is finally accepted, it is typeset and published in the journal.  If
it is not accepted, then the authors can try again and repeat the whole process
with a different journal.

\paragraph{What is the purpose of peer review\protect\footnotemark[1]}
\footnotetext[1]{
    This paragraph is mainly based on blogs containing anecdotes from various
    editors/reviewers. Main sources:
    \cite{Smith2006},
    \url{http://www.nature.com/nature/peerreview/debate/nature04990.html} and
    \url{http://www.michaeleisen.org/blog/?p=694}
}
At first glance, this system
looks good. Firstly it should work as a filter to prevent flawed research from
being published, since the reviewers, who are skilled scientists themselves,
should be able to recognize any flaws. Secondly it should prevent an
information overload by distributing the right papers to the right journals and
preventing the publication of non-novel results. Thirdly it gives authors a
chance to get objective opinions from outsiders, who might notice certain
weaknesses that the authors have not discovered themselves. Most authors
agree that, by doing so, the peer review process leads to work of higher
quality \citep{Mulligan2013}.

Unfortunately, there are many points along the road where this system can go
wrong, as it often does, as is evident from the research presented above.
In addition, there is debate if peer review is actually responsible for
detecting fraudulent research \citep{Marris2006a}. Obvious flaws in a paper
should be discovered by the reviewers, but skillfully falsified results can be
undetectable without reproducing every experiment described in the paper. Most
scientists agree that it is unreasonable to expect such a level of dedication
from reviewers \citep{Mulligan2013}.

\paragraph{Public perception of peer review} Scientists are aware of how the
peer review system works and know that peer-reviewed does not mean correct or
qualitative. The public perception on the other hand is that peer-reviewed
articles are the gold truth, supported by the scientific community
\citep{Clarke2006}. They don't know it is merely a quick check by two to three
other scientists working in the same field. This situation can be dangerous
when flawed research passes the peer-review process and is accepted as truth by
the public.

\section{How does flawed research pass the peer review process?}

The majority agree that detecting fraud is not among the responsibilities of
peer review. However, there are not many systems in place to prevent fraudulent
research from being published apart from it; institutions should of course try
to prevent fraud in the first place. It can thus be argued that, as long as you
make it a little believable, publishing fraudulent research is rather easy.
Still, obviously flawed research passes the peer review process rather easily
as well \citep{Godlee1998a,Schroter2004,Bohannon2013}. The question is then,
how does this happen?

For one, reviewers often don't have access to raw data, making it nearly
impossible to validate any statistical methods used in the paper. In addition,
if the data looks sound, there is no way for reviewers to tell if the data was
actually gathered as the article describes, instead of having been invented.
The peer review process operates on trust in this regard, which is rather
ineffective for catching fraudsters.

Secondly, reviews have to be completed within a short deadline. This benefits
the authors since it decreases the delay between submission and publication
\citep{Mulligan2013}.  But it makes it also very difficult for reviewers to
scrutinize the article in great detail. Some sort of trade-off has to be made
here: do we allow more time to assure quality or do we allow less time to
improve the speed of scientific communication? After all, sooner availability
of new results increases the rate of scientific progress, given the results are
valid of course.

Thirdly, participating in peer review has a low priority. It takes time to
conduct a good peer review and the rewards are small. Contributing to this
problem is the rapidly growing number of submissions that need peer reviewers.

Fourthly, reviewers seem to perform rather poorly at detecting even obvious
errors in methodology or interpretation. \cite{Godlee1998a} have conducted a
trial in which a paper with 8 major flaws in methods or analysis was given to
reviewers. The mean number of weaknesses found by reviewers was 2, and 16\% of
the reviewers did not find any of the mistakes. This happened despite the fact
that most of the reviewers knew they were taking part in the trial. These
results stand, unfortunately, in agreement with a more recent study as well
\citep{Schroter2004}.

The fifth and arguably biggest problem is that there are too many papers being
submitted. An impressive number of obscure journals have followed this trend,
leading to too many papers being published as well. If a submission is rejected
in one journal, it will almost certainly find a place in another journal.
\cite{Bohannon2013} did an interesting experiment in this regard and sent
papers with obvious contradictions and flaws to 304 open access journals, all
claiming to be peer reviewed. Of these, 157 accepted the paper with only minor
modifications to layout or references. Among them were a few well known
publishers such as Eslevier and Sage. Many of the journals that accepted
the manuscript seemed to use no peer review at all.

\section{What has been proposed to improve the peer review process?}

Nature started an online debate concerning peer review some time ago, along
with a series of web-articles. Quite a few of them had some good ideas about
reforming the peer review process
(\url{http://www.nature.com/nature/peerreview/debate}).  There are more ideas
that were presented in other journals as well, such as \cite{Smith2006} and
\cite{Casadevall2012}. Altogether these sources propose so many interesting
idea's that I cannot go over each one of them in detail.  Here I will present a
selection of these ideas that appealed most to me.

\paragraph{Improve transparency} Often there is no requirement to publish raw
data when submitting a paper. However, if there was such a requirement, it
would make fraud a lot more difficult. Also, data could be independently
verified by others after the peer review has finished.

Another front where transparency can be improved is the peer review process
itself.  Making the whole peer review process accessible after publication
makes it a lot easier to see where it fails. This could be a big step towards
researching peer review and improving it in the process.

Nature has experimented briefly with an even more open peer review model.
Articles were simultaneously peer reviewed in the traditional sense and made
available to all subscribers on an open peer review server. Although the idea
was received with great enthusiasm at first, not many useful comments were
posted on the open peer review server \citep{Nature2006}.

\paragraph{Stop the publication bias} Journals tend to accept positive data
much more than negative data. This gives additional motivation to authors to
make their results look positive even when experiments failed to show the
expected result. Showing that a scientifically sound experiment does not show
something can be an equally important contribution to science, as it can
prevent researchers in the future from attempting it again.

\paragraph{Limit the number of publications} There is a lot of pressure on
scientists to publish as many papers as they can as quickly as possible. This
leads to a lot of papers being published that don't really have anything to
say. Rushing a paper severely impacts the quality as well. Although extremely
difficult to implement, one could envision a system where the number of
publications per researcher per year is limited. This would take a lot of
pressure of the scientists shoulders and give them more time to produce
qualitative work. In the best case scenario, it would lead to a smaller
quantity of more informative papers, thus improving quality while at the same
time reducing the information overload.

\paragraph{Post publication amendments} Once an article is published, it is
frozen in time. When new findings even slightly invalidate previous results,
they have to be published in a new article. This has two disadvantages: firstly
anyone who reads the old article is unaware of the invalidated results, as the
old article does not reference the newer one and secondly, the whole
time-consuming and expensive peer review process has to be repeat all over
again. An alternative would be to allow multiple versions of the same article,
this again improves the quality of the article and reduces the information
overload.

\paragraph{Other ideas} There are many more ideas that are worth sharing such
\todo[size=\footnotesize]{
    I don't remember where I read this, the article mentioned group peer
    reviews similar to the ``journal clubs'' which are held in many labs today.
    I thinks it's an idea worth mentioning.
}
as journal-club style peer review, using image-analysis software to detect
fraud \citep{Pearson2006}, giving reviewers more credit for their work
\citep{Groves2008}, establishing strict journal policies for fraud
investigations \citep{Marris2006a} and organizing training sessions to improve
reviewers' awareness of scientific flaws \citep{Schroter2004}.

\section{Time for change}

Peer review is currently the cornerstone of scientific communication, and it
has been for a long time. But after digging into the subject, it is my opinion
that a serious reform is required. I think, after reading the numerous articles
mentioned above, that peer review frequently fails at nearly every aspect it
was designed for:
\begin{itemize}[noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt,leftmargin=*]
    \item It often fails to detect poor quality research and it can, due to the
        way it is designed, not detect thoughtfully falsified results.
    \item It does not prevent an information overload due to the many obscure
        journals.
    \item It does not work effectively as a distribution system. As simply
        entering a query on PubMed or Web of Science is far better suited for
        this. Almost nobody still reads a journal edition from front to back.
\end{itemize}
In addition, peer review creates a few new problems as well. I have been in
contact with a few reviewers myself, and they mentioned deliberate delaying of
publications by reviewers due to conflicts of interest (which are often not
being indicated by the reviewer) as a major problem. Also, peer review is
an incredibly expensive and time consuming process.

\paragraph{Should we keep peer review?}
I still believe that in principle peer review is not bad. What is wrong is, in
my opinion, the way it is implemented in combination with a poor
publish-or-perish mentality. We will have to address both problems if we want
see any improvement.

Let us look at the first problem. For one, peer review has to be carried out
more thoroughly, and to allow for this we must make the process of
peer-reviewing more valuable. Also, peer review is most often done by lab heads
only. Several researchers with different expertises should review a paper and
then discuss it together. I am a big fan of a journal-club style peer review;
for example lab technicians might notice flaws in methods that lab-heads did
not notice. A move in the right direction is the peer review process of eLife,
where reviewers have to discuss with each other and the editor to write one
final review (all reviewers I had contact with actually endorsed the eLife
review process). This discussion also prevents personal interests of one author
from delaying a publication. As mentioned before I am also in favor of
mandatory raw data submission with every paper and complete transparency of the
peer review process after publication.

To address the second problem, we will have to remove the pressure to publish
in high quantity. It would be interesting to restrict the number of
publications one can have per $x$ years. Then one would have to make sure that
the few publications they can have are worth it. In my field of Bioinformatics
I often have the impression that groups publish application papers for the sake
of bumping up their paper count. I have found many applications that were not
maintained and/or non-functioning only a few years after the article got
accepted. What we really need as well is a change in mentality: a researcher
should be judged by the quality of his/hers work, not the number of
publications.

By addressing these problems we might hope to see a brighter future with
overall higher quality contributions contained within fewer papers,
concurrently removing the impetus for carrying out fraud in the first place.

\section{Conclusion}

There are many flaws to the current peer review process. Some say it is the
least worst option we have and that the actual peer review only starts once the
article has been published. I am a bit more optimistic. There is much room
for improvement and the current trend to research peer review and research
misconduct can play a big role in this. Science is said to be self correcting,
and a reform of the peer review process might be the next correction it needs.

\clearpage

\section*{Further reading}

Nature started a debate on peer review in 2006, including many interesting
articles and discussions, check it out at
\url{http://www.nature.com/nature/peerreview/debate}. BMC recently announced a
new journal publishing research about research, the first publications will be
coming soon at \url{http://researchintegrityjournal.biomedcentral.com}.  If
you want to stay up to date on article retractions, then keep an eye on
\url{http://retractionwatch.com}, and also take a look at their unofficial
retraction leaderboard.

\section*{Acknowledgements}
I would like to thank Prof.\ Dr.\ Ir.\ Steven Maere and Prof.\ Dr.\ Klaas
Vandepoele for responding to my questions about peer review. I would also like
to thank Daniel Morgan (PhD) for correcting spelling and grammar mistakes.
Finally I would like to thank Prof.\ Dr.\ Peter Vandamme for helpful feedback
on this document.

\bibliographystyle{abbrvnat}
\bibliography{library}

\end{document}
