MAIN=DeMeyer_Sam_BIS_2015-2016

SRC_DIR=src
BLD_DIR=build
OUT_DIR=output

ALL_SOURCES:=$(wildcard $(SRC_DIR)/*)
BLD_LINKS_UPDATED:=$(BLD_DIR)/.links_updated_makereq

# create pdf and all it's dependencies
all: $(OUT_DIR)/$(MAIN).pdf

run: $(OUT_DIR)/$(MAIN).pdf
	xdg-open $(OUT_DIR)/$(MAIN).pdf &> /dev/null

$(OUT_DIR)/$(MAIN).pdf: $(BLD_DIR)/$(MAIN).pdf | $(OUT_DIR)
	cp $(BLD_DIR)/$(MAIN).pdf $(OUT_DIR)/$(MAIN).pdf

$(BLD_DIR)/$(MAIN).pdf: $(BLD_LINKS_UPDATED)
	touch $(BLD_DIR)/$(MAIN).pdf && \
	latexmk -cd -pdflatex=lualatex -pdf $(BLD_DIR)/$(MAIN).tex

# symlink everything in source directory to build directory
link: $(BLD_LINKS_UPDATED)

$(BLD_LINKS_UPDATED): $(ALL_SOURCES) | $(BLD_DIR)
	ln -sf $(addprefix ../, $(ALL_SOURCES)) $(BLD_DIR) && \
	touch $(BLD_LINKS_UPDATED)

# create build and output directories
directories: $(BLD_DIR) $(OUT_DIR)

$(BLD_DIR):
	mkdir $(BLD_DIR)

$(OUT_DIR):
	mkdir $(OUT_DIR)

# clean repository
clean:
	rm -rf $(BLD_DIR) $(OUT_DIR)
